(require 'cl)

;; Packages
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives
	       '("melpa" . "http://melpa.milkbox.net/packages/")
	       t))

(defvar required-packages
  '(;;General
    magit 
    helm 
    solarized-theme
    smartparens
    yasnippet
    auto-complete
    ac-helm
    ac-clang
    haskell-mode
    haskell-snippets
    clojure-mode
    cider
    clj-refactor
    rainbow-delimiters
    avy
    swift-mode
    exec-path-from-shell
    ))

(defun packages-installed-p (packages)
  (loop for p in packages
	when (not (package-installed-p p)) do (return nil)
	finally (return t)))

(unless (packages-installed-p required-packages)
  (package-refresh-contents)
  (dolist (p required-packages)
    (when (not (package-installed-p p))
      (package-install p))))

;; OSX
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))


;; Global
(global-set-key (kbd "C-x ;") 'avy-goto-char)

(require 'helm-config)
(helm-mode 1)

(require 'yasnippet)
(yas-global-mode 1)

(require 'auto-complete-config)
;; (add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)
(ac-set-trigger-key "TAB")
(ac-set-trigger-key "<tab>")


;; Display
(setq inhibit-splash-screen t)

(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

(load-theme 'solarized-light t)

(when (display-graphic-p)
  (let ((pragmata "Essential PragmataPro-16"))
    (when (member pragmata (font-family-list))
      (set-frame-font "Essential PragmataPro-16"))))

;; Keybindings

(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)

;; Magit
;; (when (eq system-type 'darwin)
;;   (setq exec-path (cons "/usr/local/git/bin" exec-path))
;;   (setq exec-path (cons "/usr/local/bin" exec-path)))

(global-set-key "\C-xg" 'magit-status)
(global-set-key "\C-cg" 'magit-status)

(define-key ac-complete-mode-map "\C-n" 'ac-next)
(define-key ac-complete-mode-map "\C-p" 'ac-previous)


;;; Languages

;; C

(require 'ac-clang)
;; (require 'flymake)
;; (add-hook 'find-file-hook 'flymake-find-file-hook)
 

;; Haskell

(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)

;; Clojure
(setenv "PATH" (concat (expand-file-name "~/bin") ":" (getenv "PATH")))
(setq exec-path (cons (expand-file-name "~/bin") exec-path))

(require 'smartparens)

(sp-use-paredit-bindings)

(add-hook 'cider-repl-mode-hook #'smartparens-strict-mode)
(add-hook 'clojure-mode-hook #'smartparens-strict-mode)
(add-hook 'cider-repl-mode-hook #'rainbow-delimiters-mode)
(add-hook 'clojure-mode-hook #'rainbow-delimiters-mode)
